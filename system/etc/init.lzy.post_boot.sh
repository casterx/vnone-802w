#!/system/bin/sh
# =============================================
# |         HTC One M7_UL Tweaks Script       |
# |                  LZY ONE                  |
# |          Tweaks & Optimizations           |
# =============================================
# |   If you want to use any of my settings   |
# |    Please mention my name and my weibo    |
# |           And feel free to use            |
# =============================================

target=`getprop ro.board.platform`

case "$target" in
    "msm8960")
        echo 1890000 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq;
        echo 1890000 > /sys/devices/system/cpu/cpu1/cpufreq/scaling_max_freq;
        echo 1890000 > /sys/devices/system/cpu/cpu2/cpufreq/scaling_max_freq;
        echo 1890000 > /sys/devices/system/cpu/cpu3/cpufreq/scaling_max_freq;
        echo "ondemand" > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
        echo "ondemand" > /sys/devices/system/cpu/cpu1/cpufreq/scaling_governor
        echo "ondemand" > /sys/devices/system/cpu/cpu2/cpufreq/scaling_governor
        echo "ondemand" > /sys/devices/system/cpu/cpu3/cpufreq/scaling_governor
        #echo "intellidemand" > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
        #echo "intellidemand" > /sys/devices/system/cpu/cpu1/cpufreq/scaling_governor
        #echo "intellidemand" > /sys/devices/system/cpu/cpu2/cpufreq/scaling_governor
        #echo "intellidemand" > /sys/devices/system/cpu/cpu3/cpufreq/scaling_governor
        #echo 60 > /sys/devices/system/cpu/cpufreq/intellidemand/powersave_bias
        echo 90 > /sys/devices/system/cpu/cpufreq/ondemand/up_threshold
        echo 50000 > /sys/devices/system/cpu/cpufreq/ondemand/sampling_rate
        echo 1 > /sys/devices/system/cpu/cpufreq/ondemand/io_is_busy
        echo 4 > /sys/devices/system/cpu/cpufreq/ondemand/sampling_down_factor
        echo 10 > /sys/devices/system/cpu/cpufreq/ondemand/down_differential
        echo 70 > /sys/devices/system/cpu/cpufreq/ondemand/up_threshold_multi_core
        echo 3 > /sys/devices/system/cpu/cpufreq/ondemand/down_differential_multi_core
        echo 918000 > /sys/devices/system/cpu/cpufreq/ondemand/optimal_freq
        echo 918000 > /sys/devices/system/cpu/cpufreq/ondemand/sync_freq
        echo 80 > /sys/devices/system/cpu/cpufreq/ondemand/up_threshold_any_cpu_load
        echo 384000 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq
        echo 384000 > /sys/devices/system/cpu/cpu1/cpufreq/scaling_min_freq
        echo 384000 > /sys/devices/system/cpu/cpu2/cpufreq/scaling_min_freq
        echo 384000 > /sys/devices/system/cpu/cpu3/cpufreq/scaling_min_freq
	echo 4096 > /proc/sys/vm/min_free_kbytes
	echo "16 16" > /proc/sys/vm/lowmem_reserve_ratio
	echo row > /sys/block/mmcblk0/queue/scheduler
        chown system /sys/devices/system/cpu/cpufreq/ondemand/io_is_busy
        chown system /sys/devices/system/cpu/cpufreq/ondemand/sampling_rate
        chown system /sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq
        chown system /sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq
        chown system /sys/devices/system/cpu/cpu1/cpufreq/scaling_max_freq
        chown system /sys/devices/system/cpu/cpu1/cpufreq/scaling_min_freq
        chown system /sys/devices/system/cpu/cpu2/cpufreq/scaling_max_freq
        chown system /sys/devices/system/cpu/cpu2/cpufreq/scaling_min_freq
        chown system /sys/devices/system/cpu/cpu3/cpufreq/scaling_max_freq
        chown system /sys/devices/system/cpu/cpu3/cpufreq/scaling_min_freq
        chown root.system /sys/devices/system/cpu/mfreq
        chmod 220 /sys/devices/system/cpu/mfreq
        chown root.system /sys/devices/system/cpu/cpu1/online
        chown root.system /sys/devices/system/cpu/cpu2/online
        chown root.system /sys/devices/system/cpu/cpu3/online
        chmod 664 /sys/devices/system/cpu/cpu1/online
        chmod 664 /sys/devices/system/cpu/cpu2/online
        chmod 664 /sys/devices/system/cpu/cpu3/online
    ;;
esac

# Post-setup services
case "$target" in
    "msm8960")
	start adaptive

#For Faux Kernel
#	stop mpdecision
#echo 1 > /sys/module/intelli_plug/parameters/intelli_plug_active
    ;;
esac

target=`getprop ro.ident`
case "$target" in "LZY_ONE")

# Kernel Tweaks
mount -t debugfs none /sys/kernel/debug
echo "NO_NORMALIZED_SLEEPER" > /sys/kernel/debug/sched_features
echo "NO_NEW_FAIR_SLEEPERS" > /sys/kernel/debug/sched_features
#umount /sys/kernel/debug
echo 0 > /proc/sys/kernel/panic_on_oops

# Virtual Memory tweaks
echo 1 > /proc/sys/vm/oom_kill_allocating_task;
echo 0 > /proc/sys/vm/panic_on_oom;

# Network tweaks
#echo 1 > /proc/sys/net/ipv4/tcp_tw_reuse;
#echo 1 > /proc/sys/net/ipv4/tcp_sack;
#echo 1 > /proc/sys/net/ipv4/tcp_tw_recycle;
#echo 0 > /proc/sys/net/ipv4/tcp_window_scaling;
#echo 5 > /proc/sys/net/ipv4/tcp_keepalive_probes;
#echo 30 > /proc/sys/net/ipv4/tcp_keepalive_intvl;
#echo 30 > /proc/sys/net/ipv4/tcp_fin_timeout;
#echo 1200 > /proc/sys/net/ipv4/tcp_keepalive_time;
#echo 524288 > /proc/sys/net/core/wmem_max;
#echo 524288 > /proc/sys/net/core/rmem_max;

# Enable Pocket Detection
echo 1 > /sys/android_touch/pocket_detect

# Increase Cache size to reduce power consumption
echo 2048 > /sys/block/mmcblk0/queue/read_ahead_kb
echo 2048 > /sys/block/mmcblk1/queue/read_ahead_kb

# Fast Charge
echo 1 > /sys/kernel/fast_charge/force_fast_charge

# L2M/L2W/L2S/H2W/S2W Settings
echo 1 > /sys/android_touch/doubletap2wake
#echo 3 > /sys/android_touch/home2wake
echo 1 > /sys/android_touch/logo_delay
echo 0 > /sys/android_touch/logo2menu
echo 1 > /sys/android_touch/logo2wake
echo 1 > /sys/android_touch/sweep2wake

# Deep sleep states tweaks for krait 300 soc
echo 1 > /sys/module/pm_8x60/modes/cpu0/wfi/idle_enabled
echo 1 > /sys/module/pm_8x60/modes/cpu1/wfi/idle_enabled
echo 1 > /sys/module/pm_8x60/modes/cpu2/wfi/idle_enabled
echo 1 > /sys/module/pm_8x60/modes/cpu3/wfi/idle_enabled
#echo 0 > /sys/module/pm_8x60/modes/cpu0/retention/idle_enabled
#echo 0 > /sys/module/pm_8x60/modes/cpu1/retention/idle_enabled
#echo 0 > /sys/module/pm_8x60/modes/cpu2/retention/idle_enabled
#echo 0 > /sys/module/pm_8x60/modes/cpu3/retention/idle_enabled
echo 1 > /sys/module/pm_8x60/modes/cpu0/standalone_power_collapse/suspend_enabled
echo 1 > /sys/module/pm_8x60/modes/cpu1/standalone_power_collapse/suspend_enabled
echo 1 > /sys/module/pm_8x60/modes/cpu2/standalone_power_collapse/suspend_enabled
echo 1 > /sys/module/pm_8x60/modes/cpu3/standalone_power_collapse/suspend_enabled
echo 1 > /sys/module/pm_8x60/modes/cpu0/standalone_power_collapse/idle_enabled
echo 1 > /sys/module/pm_8x60/modes/cpu1/standalone_power_collapse/idle_enabled
echo 1 > /sys/module/pm_8x60/modes/cpu2/standalone_power_collapse/idle_enabled
echo 1 > /sys/module/pm_8x60/modes/cpu3/standalone_power_collapse/idle_enabled
echo 1 > /sys/module/pm_8x60/modes/cpu0/power_collapse/suspend_enabled
echo 1 > /sys/module/pm_8x60/modes/cpu1/power_collapse/suspend_enabled
echo 1 > /sys/module/pm_8x60/modes/cpu2/power_collapse/suspend_enabled
echo 1 > /sys/module/pm_8x60/modes/cpu3/power_collapse/suspend_enabled
echo 1 > /sys/module/pm_8x60/modes/cpu0/power_collapse/idle_enabled
echo 1 > /sys/module/pm_8x60/modes/cpu1/power_collapse/idle_enabled
echo 1 > /sys/module/pm_8x60/modes/cpu2/power_collapse/idle_enabled
echo 1 > /sys/module/pm_8x60/modes/cpu3/power_collapse/idle_enabled

# Low Power Tweaks
echo 1 > /sys/module/rpm_resources/enable_low_power/L2_cache
echo 1 > /sys/module/rpm_resources/enable_low_power/pxo
echo 1 > /sys/module/rpm_resources/enable_low_power/vdd_dig
echo 1 > /sys/module/rpm_resources/enable_low_power/vdd_mem

# GPU OC
echo 450000000 > /sys/devices/platform/kgsl-3d0.0/kgsl/kgsl-3d0/max_gpuclk

# Blink Button
echo 1 > /sys/class/leds/button-backlight/blink_buttons

# Disable eMMC Entropy
echo 0 > /sys/block/mmcblk0/queue/add_random
echo 0 > /sys/block/mmcblk1/queue/add_random

# Thermal throttling settings for ElementalX kernel
# Cool
#echo 58,64,75,85 > /sys/module/msm_thermal/parameters/limit_temp_degC
# Extra Cool
echo 54,60,75,85 > /sys/module/msm_thermal/parameters/limit_temp_degC
# Hot
#echo 66,71,75,85 > /sys/module/msm_thermal/parameters/limit_temp_degC



